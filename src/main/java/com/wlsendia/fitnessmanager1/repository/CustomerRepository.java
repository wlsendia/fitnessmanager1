package com.wlsendia.fitnessmanager1.repository;

import com.wlsendia.fitnessmanager1.entity.PersonalTrainingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository<PersonalTrainingCustomer, Long> {
}
