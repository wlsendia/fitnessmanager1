package com.wlsendia.fitnessmanager1.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {

    //전체 출력
    private String customerName;

    private String customerPhone;

    private Float height;

    private Float weight;

    private Float bmi;

    private String obesity;

    private LocalDate joinDate;

    private LocalDateTime visitDate;
}
