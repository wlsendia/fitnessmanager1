package com.wlsendia.fitnessmanager1.service;

import com.wlsendia.fitnessmanager1.entity.PersonalTrainingCustomer;
import com.wlsendia.fitnessmanager1.model.CustomerInfoUpdateRequest;
import com.wlsendia.fitnessmanager1.model.CustomerItem;
import com.wlsendia.fitnessmanager1.model.CustomerRequest;
import com.wlsendia.fitnessmanager1.model.CustomerWeightUpdateRequest;
import com.wlsendia.fitnessmanager1.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;

    public void setCustomer(CustomerRequest request) {
        PersonalTrainingCustomer addData = new PersonalTrainingCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setHeight(request.getHeight());
        addData.setWeight(request.getWeight());
        addData.setJoinDate(LocalDate.now());//없을 경우만 추가하게 수정

        repository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = new LinkedList<>();

        List<PersonalTrainingCustomer> origin = repository.findAll();

        for (PersonalTrainingCustomer list : origin) {
            CustomerItem temp = new CustomerItem();
            temp.setCustomerName(list.getCustomerName());
            temp.setCustomerPhone(list.getCustomerPhone());
            temp.setWeight(list.getWeight());
            temp.setHeight(list.getHeight());
            temp.setJoinDate(list.getJoinDate());

            Float bmi = list.getHeight() / (list.getWeight() * list.getWeight() / 10000);
            temp.setBmi(bmi);

            String obesity;
            if (bmi >= 30) obesity = "30이상";
            else if (bmi >= 25) obesity = "25이상";
            else if (bmi >= 20) obesity = "20이상";
            else obesity = "20미만";

            temp.setObesity(obesity);

            result.add(temp);
        }


        return result;

    }
    public void putCustomerInfo(CustomerInfoUpdateRequest request, long id){
        PersonalTrainingCustomer temp = repository.findById(id).orElseThrow();
        if (request.getCustomerName()!="")
            temp.setCustomerName(request.getCustomerName());//빈칸일경우갱신x기능넣기
        if(request.getCustomerPhone()!="")
            temp.setCustomerPhone(request.getCustomerPhone());

        repository.save(temp);
    }
    public void putCustomerWeight(CustomerWeightUpdateRequest request, long id){
        PersonalTrainingCustomer temp = repository.findById(id).orElseThrow();
        temp.setWeight(request.getWeight());

        repository.save(temp);
    }

    public void putCustomerVisit(long id){
        PersonalTrainingCustomer temp = repository.findById(id).orElseThrow();
        temp.setVisitDate(LocalDateTime.now());

        repository.save(temp);
    }
}
