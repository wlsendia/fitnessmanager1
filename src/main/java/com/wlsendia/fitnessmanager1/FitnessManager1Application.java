package com.wlsendia.fitnessmanager1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessManager1Application {

    public static void main(String[] args) {
        SpringApplication.run(FitnessManager1Application.class, args);
    }

}
