package com.wlsendia.fitnessmanager1.controller;

import com.wlsendia.fitnessmanager1.model.CustomerInfoUpdateRequest;
import com.wlsendia.fitnessmanager1.model.CustomerItem;
import com.wlsendia.fitnessmanager1.model.CustomerRequest;
import com.wlsendia.fitnessmanager1.model.CustomerWeightUpdateRequest;
import com.wlsendia.fitnessmanager1.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService service;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request){
        service.setCustomer(request);

        return "ok1";
    }

    @GetMapping("/people")
    public List<CustomerItem> getCustomers(){
        List<CustomerItem> result = new LinkedList<>();
        result = service.getCustomers();

        return result;
    }

    @PutMapping("/info/id/{id}")
    public String putCustomerInfo(@RequestBody @Valid CustomerInfoUpdateRequest request,
                                  @PathVariable long id){
        service.putCustomerInfo(request, id);

        return "ok2";
    }

    @PutMapping("/weight/id/{id}")
    public String putCustomerWeight(@RequestBody @Valid CustomerWeightUpdateRequest request,
                                    @PathVariable long id){
        service.putCustomerWeight(request, id);

        return "ok3";
    }
    @PutMapping("/visit/id/{id}")
    public String putCustomerVisit(@PathVariable long id){
        service.putCustomerVisit(id);

        return "ok4";
    }
}
// 출력 명단에 과체중여부 추가
// 등록, 갱신을 둘 다 만들어얃 ㅚㅁ